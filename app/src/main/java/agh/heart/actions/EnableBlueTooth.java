package agh.heart.actions;

import android.bluetooth.BluetoothAdapter;

import heart.Action;
import heart.State;


public class EnableBlueTooth implements Action {
    @Override
    public void execute(State state) {
        BluetoothAdapter.getDefaultAdapter().enable();
    }
}
