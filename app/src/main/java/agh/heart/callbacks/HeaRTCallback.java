package agh.heart.callbacks;

import android.content.ContentResolver;


public class HeaRTCallback {
    static ContentResolver resolver = null;

    public static void register(ContentResolver resolver) {
        HeaRTCallback.resolver = resolver;
    }

    public static void unregister() {
        resolver = null;
    }
}
