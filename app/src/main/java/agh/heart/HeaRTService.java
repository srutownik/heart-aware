package agh.heart;

import android.os.AsyncTask;
import android.util.Log;

import com.firebase.jobdispatcher.JobParameters;
import com.firebase.jobdispatcher.JobService;

import agh.heart.model.ModelWrapper;


public class HeaRTService extends JobService {
    private AsyncTask<JobParameters, Void, JobParameters> modelExecutor = new ModelExecutor();

    @Override
    public boolean onStartJob(JobParameters job) {
        Log.d("HeaRTService", "job started");
        modelExecutor.execute(job);
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters job) {
        return false;
    }

    private class ModelExecutor extends AsyncTask<JobParameters, Void, JobParameters> {
        private ModelWrapper model = new ModelWrapper();

        @Override
        protected JobParameters doInBackground(JobParameters... params) {
            model.runInference();
            return params[0];
        }

        @Override
        protected void onPostExecute(JobParameters jobParameters) {
            jobFinished(jobParameters, false);
            Log.d("HeaRTService", "job ended");
        }
    }
}
